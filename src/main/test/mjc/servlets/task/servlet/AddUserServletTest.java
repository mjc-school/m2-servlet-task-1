package mjc.servlets.task.servlet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.Test;

import mjc.servlets.task.User;
import mjc.servlets.task.Warehouse;

class AddUserServletTest {

  @Test
  public void testServletHasAnnotation() {
    Annotation actual = AddUserServlet.class.getAnnotation(WebServlet.class);
    assertNotEquals(null, actual, "Servlet should have WebServlet annotation");
  }

  @Test
  public void testServletInheritedFromHttpServlet() {
    Class actual = AddUserServlet.class.getSuperclass();
    assertEquals(HttpServlet.class, actual, "Servlet should be inherited from HttpServlet");
  }

  private Method getMethod(String method) {
    String reason = "";
    if (method.equals("doGet")) {
      reason = "for navigating to /add page";
    } else if (method.equals("doPost")) {
      reason = "for adding a new user";
    }
    Method actual = null;
    try {
      actual =
          AddUserServlet.class.getDeclaredMethod(
              method, HttpServletRequest.class, HttpServletResponse.class);
      actual.setAccessible(true);
    } catch (NoSuchMethodException e) {
      assertTrue(false, "Servlet should have " + method + " method implemented " + reason);
    }
    return actual;
  }

  @Test
  public void testForwardToAddPage() throws Exception {
    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    RequestDispatcher dispatcher = mock(RequestDispatcher.class);
    AddUserServlet servlet = new AddUserServlet();

    when(request.getRequestDispatcher("jsp/add.jsp")).thenReturn(dispatcher);

    Method doGet = getMethod("doGet");
    doGet.invoke(servlet, request, response);

    verify(dispatcher).forward(request, response);
  }

  @Test
  public void testAddUser() throws Exception {
    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    RequestDispatcher dispatcher = mock(RequestDispatcher.class);
    AddUserServlet servlet = new AddUserServlet();
    Warehouse warehouse = Warehouse.getInstance();
    User user = new User("John", "Doe");

    when(request.getParameter("firstName")).thenReturn(user.getFirstName());
    when(request.getParameter("lastName")).thenReturn(user.getLastName());
    when(request.getRequestDispatcher(anyString())).thenReturn(dispatcher);

    Method doPost = getMethod("doPost");
    doPost.invoke(servlet, request, response);

    assertTrue(warehouse.getUsers().contains(user), "You should save a user in Warehouse");
    verify(request).setAttribute("user", user);
    verify(dispatcher).forward(request, response);
  }
}
