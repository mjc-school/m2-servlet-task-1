# Servlets

## Description:

An application has a starter page with 2 buttons: "Add User" and "Users". You need to create handlers for them. For that
create 2 servlets:

- AddUserServlet
- GetUsersServlet

## Details

<b>AddUserServlet</b> should:

1. placed in com.mjc.task.servlet package
2. have url /add
3. should navigate to /add page
4. when adding a new user should:
    - receive 2 params: firstName, lastName
    - save user in [Warehouse](src/main/java/mjc/servlets/task/Warehouse.java)
    - return request attribute "user"
    - navigate to /add page

<b>GetUsersServlet</b> should:

1. placed in com.mjc.task.servlet package
2. have url /users
3. receive users from [Warehouse](src/main/java/mjc/servlets/task/Warehouse.java)
4. as a result should:
    - return request attribute "users"
    - navigate to /users page 
