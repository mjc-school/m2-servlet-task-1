package mjc.servlets.task.servlet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.Test;

class GetUsersServletTest {

  @Test
  public void testServletHasAnnotation() {
    Annotation actual = GetUsersServlet.class.getAnnotation(WebServlet.class);
    assertNotEquals(null, actual, "Servlet should have WebServlet annotation");
  }

  @Test
  public void testServletInheritedFromHttpServlet() {
    Class actual = GetUsersServlet.class.getSuperclass();
    assertEquals(HttpServlet.class, actual, "Servlet should be inherited from HttpServlet");
  }

  private Method getDoGetMethod() {
    Method actual = null;
    try {
      actual =
          GetUsersServlet.class.getDeclaredMethod(
              "doGet", HttpServletRequest.class, HttpServletResponse.class);
      actual.setAccessible(true);
    } catch (NoSuchMethodException e) {
      assertTrue(false, "Servlet should have doGet method implemented for displaying users");
    }
    return actual;
  }

  @Test
  public void testGetUsers() throws Exception {
    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    RequestDispatcher dispatcher = mock(RequestDispatcher.class);
    GetUsersServlet servlet = new GetUsersServlet();

    when(request.getRequestDispatcher("jsp/users.jsp")).thenReturn(dispatcher);

    Method doGet = getDoGetMethod();
    doGet.invoke(servlet, request, response);

    verify(dispatcher).forward(request, response);
  }
}
